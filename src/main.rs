use fastrand;
use nannou::color::named::{CRIMSON, DARKRED, INDIANRED};
use nannou::prelude::*;

use std::time::Duration;

#[derive(Copy, Clone, Debug, Eq, PartialEq)]
enum Fuel {
    Empty,
    Tree,
    Burning(u8),
}

#[derive(Copy, Clone, Debug, PartialEq)]
struct Plot {
    x: f32,
    y: f32,
    fuel: Fuel,
    prob_ignite: u8,
    updated: bool,
}

impl Plot {
    fn update(&mut self, neighbours: &[Plot]) {
        for neighbour in neighbours {
            self.updated = false;
            self.fuel = match (self.fuel, neighbour.fuel) {
                (Fuel::Empty, _) => continue,
                (Fuel::Burning(0), _) => {
                    self.updated = true;
                    Fuel::Empty
                }
                (Fuel::Burning(turns_left), _) => {
                    self.updated = true;
                    Fuel::Burning(turns_left - 1)
                }
                (Fuel::Tree, Fuel::Burning(_)) => {
                    if fastrand::u8(..255) < self.prob_ignite {
                        self.updated = true;
                        Fuel::Burning(5)
                    } else {
                        Fuel::Tree
                    }
                }
                (_, _) => continue,
            };
        }
    }
}

struct Land {
    width: usize,
    height: usize,
    plots: Vec<Vec<Plot>>,
    plot_size: usize,
}

impl Land {
    fn new(
        width: usize,
        height: usize,
        fire_start: (usize, usize),
        prob_ignite: f32,
        plot_size: usize,
    ) -> Land {
        assert!(prob_ignite > 0.0);
        assert!(prob_ignite <= 1.0);
        assert!(width % plot_size == 0);
        assert!(height % plot_size == 0);
        assert!(fire_start.0 % plot_size == 0);
        assert!(fire_start.1 % plot_size == 0);

        let num_plots_x = width / plot_size;
        let num_plots_y = height / plot_size;

        let fire_start_x = fire_start.0 / plot_size;
        let fire_start_y = fire_start.0 / plot_size;

        let mut rows: Vec<Vec<Plot>> = Vec::with_capacity(num_plots_x);

        for x in 0..num_plots_x {
            let mut column: Vec<Plot> = Vec::with_capacity(num_plots_y);

            for y in 0..num_plots_y {
                let mut plot = Plot {
                    x: (x as f32) * (plot_size as f32),
                    y: (y as f32) * (plot_size as f32),
                    prob_ignite: (prob_ignite * 255.0).ceil() as u8,
                    fuel: Fuel::Tree,

                    updated: false,
                };

                if (x * plot_size, y * plot_size) == fire_start {
                    plot.fuel = Fuel::Burning(5);
                } else if x == 0 || y == 0 || x == num_plots_x - 1 || y == num_plots_y - 1 {
                    plot.fuel = Fuel::Empty;
                };

                column.push(plot);
            }
            rows.push(column);
        }

        Land {
            plots: rows,
            width,
            height,
            plot_size,
        }
    }

    fn update(&mut self) {
        let num_plots_x = self.width / self.plot_size;
        let num_plots_y = self.height / self.plot_size;

        for i in 1..num_plots_x - 1 {
            for j in 1..num_plots_y - 1 {
                let east = Some(self.plots[i][j + 1]);
                let south = Some(self.plots[i + 1][j]);
                let north = Some(self.plots[i - 1][j]);
                let west = Some(self.plots[i][j - 1]);

                let neighbours: Vec<_> = [north, east, west, south].into_iter().flatten().collect();

                let mut plot = self.plots[i][j];
                plot.update(&neighbours);
                self.plots[i][j] = plot;
            }
        }
    }
}
struct Model {
    land: Land,
    window_id: window::Id,
}

impl Model {
    fn update(&mut self) {
        self.land.update();
    }
}

fn view(app: &App, model: &Model, frame: Frame) {
    let draw = app.draw();

    let x_offset = (app.window_rect().w() / 2.0) - (model.land.plot_size as f32);
    let y_offset =
        (app.window_rect().h() / 2.0) - (model.land.height as f32) - (model.land.plot_size as f32);

    for row in model.land.plots.iter() {
        for plot in row.iter() {
            if frame.nth() > 0 && !plot.updated {
                continue;
            }
            let colour = match plot.fuel {
                Fuel::Empty => rgb(180, 185, 177),
                Fuel::Tree => rgb(51, 180, 51),
                Fuel::Burning(0) => DARKRED,
                Fuel::Burning(1) => INDIANRED,
                Fuel::Burning(_) => CRIMSON,
            };

            // want to draw in the top left of screen
            // to do so, need to grab retrieve the width/height of current window
            // and offset every rectangle by half of the width

            draw.rect()
                .w_h(model.land.plot_size as f32, model.land.plot_size as f32)
                .x((plot.x as f32) - x_offset + (model.land.plot_size as f32 / 2.0))
                .y((plot.y as f32) + y_offset + (model.land.plot_size as f32 / 2.0))
                .color(colour);
        }
    }

    draw.to_frame(app, &frame).unwrap();
}

fn update(_app: &App, model: &mut Model, _update: Update) {
    model.update();
}

fn init(app: &App) -> Model {
    let window_id = app.new_window().view(view).build().unwrap();

    // ensure the land dimensions, and fire start are divisible by the plot_size
    // application will panic otherwise
    let land = Land::new(500, 500, (40, 32), 0.45, 4);

    Model { land, window_id }
}

fn main() {
    println!("Hello, world!");

    nannou::app(init)
        .update(update)
        .size(1000, 1000)
        .loop_mode(LoopMode::Rate {
            // 500million nano second = half a second
            update_interval: (Duration::new(0, 500000000)),
        })
        .run();
}
